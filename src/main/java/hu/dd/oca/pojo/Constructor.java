package hu.dd.oca.pojo;

public class Constructor{
    public static void main(String[] args) { 
		Constructor cons = new Constructor();
        //pass.testReferenceType();
        //pass.testPrimitiveType();
    } 
    

    int szam;
    {System.out.println("beállítottam ezt a mezőt.");} //Alapból inicializálom (olyan mint ha a konstruktorban adtam volna értéket.)

    static{
        System.out.println("Statikus blokk.");
    }

    public Constructor(){
        System.out.println("Most fut a konstruktor.");
    }
}

