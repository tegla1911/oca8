package hu.dd.oca.pojo;

public class Final{
    private static final String testString = "ABCD";

    public void testFinal(){
        System.out.println("A final string értéke kezdetkor: " + testString);
    }

    private void changeFinalValue(){
       // testString = "asd"; //fordítás idejű hiba.
    }
}