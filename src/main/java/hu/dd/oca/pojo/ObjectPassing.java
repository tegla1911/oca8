package hu.dd.oca.pojo;

public class ObjectPassing{
    public static void main(String[] args) { 
		ObjectPassing pass = new ObjectPassing();
        //pass.testReferenceType();
        pass.testPrimitiveType();
	} 
    
    public void testReferenceType(){
        StringBuilder sb = new StringBuilder("1234");
        System.out.println("StringBuilder értéke előszőr:" + sb.toString());
        changeReference(sb);
        System.out.println("StringBuilder értéke harmadszor:" + sb.toString());
    }

    private void changeReference(StringBuilder sb){
        sb = new StringBuilder("5678");
        System.out.println("StringBuilder értéke másodszor:" + sb.toString());
    }

    public void testPrimitiveType(){
        int one = 1;
        System.out.println("Int értéke előszőr:" + one);
        changePrimitive(one);
        System.out.println("Int értéke harmadszor:" + one);
    }

    private void changePrimitive(int one) {
        one = 2;
        System.out.println("Int értéke másodszor:" + one);
    }


    //Primitív típusok érték szerint, minden más referencia szerint

    // A paraméter-átadás érték szerint történik. Amikor meghívunk egy metódust vagy egy konstruktort, akkor a metódus megkapja az érték másolatát.
    // Amikor a paraméter referencia típusú, akkor a referencián keresztül ugyanazt az objektumot érhetjük el, mivel csak a referencia értéke másolódott át:
    //  meghívhatjuk az objektumok metódusait és módosíthatjuk az objektum publikus változóit.

    //http://www.informatika-programozas.hu/informatika_java_programozas_elmelet_mutato.html
    //http://www.informatika-programozas.hu/informatika_java_programozas_gyakorlat_2_parameter_3.html
}